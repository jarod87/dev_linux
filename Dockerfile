FROM php:5.6-apache

ENV TERM=xterm

MAINTAINER Daniel Toth <firius87@gmail.com>

RUN apt-get update && apt-get -y upgrade \
	&& apt-get install --no-install-recommends -y vim nano joe mc wget bash zsh git htop xterm

RUN pecl install xdebug xdebug

RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true

RUN apt-get remove -y git

COPY ./.zshrc /root/.zshrc

RUN apt-get -y autoremove && apt-get -y autoclean \
	&& rm -rf /tmp/* /var/cache/apk/*